use criterion::{black_box, criterion_group, criterion_main, Criterion};
use squid_payments::csv_util::process_csv;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("process_csv_larger", |b| b.iter(|| process_csv("tests/transactions_large.csv").unwrap()));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);