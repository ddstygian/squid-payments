use tempdir::TempDir;
use std::path::Path;
use csv::{ByteRecord, Reader, StringRecord, Trim, WriterBuilder};
use std::fs::File;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use crossbeam_channel::Receiver;
use rayon::iter::{ParallelBridge, ParallelIterator};
use crate::account::Account;
use crate::BoxedWriter;
use crate::error::Error;
use crate::transaction::TransactionRecord;

pub fn get_csv_reader(path: &Path) -> csv::Result<Reader<File>> {
    csv::ReaderBuilder::new()
        .flexible(true)
        .trim(Trim::All)
        .from_path(path)
}

pub fn partition(file_path: &Path, partition_dir: &Path) -> Result<(), Error> {
    log::info!("Partitioning file {}, output: {}", file_path.display(), partition_dir.display());

    let (mut rdr, mut raw_record, headers) = initialise_raw_csv(file_path)?;
    let mut writers: HashMap<Vec<u8>, BoxedWriter> = HashMap::new();

    while rdr.read_byte_record(&mut raw_record)? {
        let column = &raw_record[1];
        let key = column;

        let mut entry = writers.entry(key.to_vec());
        let wtr = match entry {
            Entry::Occupied(ref mut occupied) => occupied.get_mut(),
            Entry::Vacant(vacant) => {
                let key = std::str::from_utf8(key)?;
                let file_name = partition_dir.join(&format!("{}.csv", key));

                log::info!("Initialised writer with name {}", file_name.display());

                // Create the file and write the header
                let file = std::fs::File::create(file_name)?;
                let mut wtr = csv::Writer::from_writer(Box::new(file));
                wtr.write_record(&headers)?;
                vacant.insert(wtr)
            }
        };

        wtr.write_byte_record(&raw_record)?;
    }
    for (_, wtr) in writers.iter_mut() {
        wtr.flush()?;
    }
    Ok(())
}

pub fn initialise_raw_csv(file_path: &Path) -> Result<(Reader<File>, ByteRecord, ByteRecord), Error> {
    let mut rdr = get_csv_reader(file_path)?;
    let raw_record = csv::ByteRecord::new();
    let headers = rdr.byte_headers()?.clone();
    Ok((rdr, raw_record, headers))
}

pub fn initialise_string_csv(path: &Path) -> Result<(Reader<File>, StringRecord, StringRecord), Error> {
    let mut rdr = get_csv_reader(path)?;
    let record = csv::StringRecord::new();
    let headers = rdr.headers()?.clone();

    Ok((rdr, record, headers))
}

pub fn process_csv(path: &str) -> Result<(), Error> {
    log::debug!("Processing CSV file: {}", path);

    // First we partition
    let temp_dir = TempDir::new("squid_payments")?;
    log::debug!("Temp dir: {}", temp_dir.path().display());

    partition(Path::new(path), temp_dir.path())?;

    let (tx, rx) = crossbeam_channel::unbounded();
    writer_manager(rx);

    let paths = glob::glob(&temp_dir.path().join("*.csv").display().to_string())
        .expect("Failed to read glob pattern");
    paths
        .par_bridge()
        .filter_map(|entry| match entry {
            Ok(path) => Some(path),
            Err(err) => {
                log::error!("Could not read glob pattern {}", err);
                None
            }
        })
        .for_each(|path| {
            match initialise_string_csv(&path) {
                Ok((mut rdr, mut raw_record, headers)) => {

                    let mut account = Account::default();

                    while rdr.read_record(&mut raw_record).expect("Invalid record") {
                        if let Ok(record) = raw_record.deserialize::<TransactionRecord>(Some(&headers)) {
                            if account.client == 0 {
                                account.client = record.client
                            }

                            let additional_tx = match record.kind.to_lowercase().as_str() {
                                "dispute" => {
                                    crate::find_additional_transaction(&path, record.tx)
                                }
                                "resolve" => {
                                    crate::find_additional_transaction(&path, record.tx)
                                }
                                "chargeback" => {
                                    crate::find_additional_transaction(&path, record.tx)
                                }
                                _ => Ok(None)
                            };
                            if let Ok(additional_tx) = additional_tx {
                                account.process(&record, additional_tx).ok();
                            }
                        }
                    }
                    if let Err(err) = tx.send(account) {
                        log::error!("Failed to send to channel {}", err);
                    }
                }
                Err(err) => {
                    log::error!("Failed to get a reader {:?}", err);
                }
            }
        });

    temp_dir.close()?;
    Ok(())
}

fn writer_manager(rx: Receiver<Account>) {
    rayon::spawn(move || {
        let mut wtr = csv::Writer::from_writer(std::io::stdout());
        let mut headers_written = false;

        while let Ok(account) = rx.recv() {
            wtr.serialize(account).ok();
            if !headers_written {
                headers_written = true;
                wtr.flush().ok();
                wtr = WriterBuilder::new()
                    .has_headers(false)
                    .from_writer(std::io::stdout());
            }
        }
        wtr.flush().ok();
    });
}
