use squid_payments::csv_util::process_csv;

fn main() -> Result<(), anyhow::Error> {
    pretty_env_logger::init();
    let path = {
        let args: Vec<String> = std::env::args().collect();
        args[1].clone()
    };

    process_csv(&path)?;

    Ok(())
}
