use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct TransactionRecord {
    #[serde(rename = "type")]
    pub kind: String,
    pub client: u16,
    pub tx: u32,
    pub amount: Option<f32>,
}

impl TransactionRecord {
    pub fn deposit(client: u16, tx: u32, amount: f32) -> TransactionRecord {
        TransactionRecord {
            kind: "deposit".to_string(),
            client,
            tx,
            amount: Some(amount),
        }
    }

    pub fn withdrawal(client: u16, tx: u32, amount: f32) -> TransactionRecord {
        TransactionRecord {
            kind: "withdraw".to_string(),
            client,
            tx,
            amount: Some(amount),
        }
    }

    pub fn dispute(client: u16, tx: u32) -> TransactionRecord {
        TransactionRecord {
            kind: "dispute".to_string(),
            client,
            tx,
            amount: None
        }
    }

    pub fn resolve(client: u16, tx: u32) -> TransactionRecord {
        TransactionRecord {
            kind: "resolve".to_string(),
            client,
            tx,
            amount: None
        }
    }

    pub fn chargeback(client: u16, tx: u32) -> TransactionRecord {
        TransactionRecord {
            kind: "chargeback".to_string(),
            client,
            tx,
            amount: None
        }
    }
}