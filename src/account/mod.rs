use serde::{Serialize, Deserialize};
use crate::error::Error;
use crate::transaction::{TransactionRecord};

#[derive(Debug, Deserialize, Serialize)]
pub struct Account {
    pub client: u16,
    pub available: f32,
    pub held: f32,
    pub total: f32,
    pub locked: bool,
    #[serde(skip_serializing)]
    pub dispute_counter: u16,
}

impl Default for Account {
    fn default() -> Self {
        Account {
            client: 0,
            available: 0.0,
            held: 0.0,
            total: 0.0,
            locked: false,
            dispute_counter: 0,
        }
    }
}

impl Account {
    pub fn new(client: &u16) -> Account {
        Account {
            client: *client,
            ..Default::default()
        }
    }

    pub fn process(&mut self, tx: &TransactionRecord, additional_tx: Option<TransactionRecord>) -> Result<(), Error> {
        match tx.kind.to_lowercase().as_str() {
            "deposit" => {
                self.available += tx.amount.unwrap_or_default();
            }
            "withdrawal" => {
                self.available -= tx.amount.unwrap_or_default();
            }
            "dispute" => {
                let amt = Self::amount_from_optional_tx(&additional_tx)?;
                self.available -= amt;
                self.held += amt;
                self.dispute_counter += 1;
            }
            "resolve" => {
                let amt = Self::amount_from_optional_tx(&additional_tx)?;
                self.held -= amt;
                self.available += amt;
                if self.dispute_counter > 0 {
                    self.dispute_counter -= 1;
                }
            }
            "chargeback" => {
                let amt = Self::amount_from_optional_tx(&additional_tx)?;
                self.held -= amt;
                self.available -= amt;
                if self.dispute_counter > 0 {
                    self.dispute_counter -= 1;
                }
            }
            x => {
                log::warn!("Unexpected dispute type {}", x)
            }
        }
        self.calculate_total();
        self.calculate_locked();
        Ok(())
    }

    fn amount_from_optional_tx(additional_tx: &Option<TransactionRecord>) -> Result<f32, Error> {
        additional_tx.as_ref()
            .ok_or(Error::AdditionalTxMissing)?
            .amount
            .ok_or(Error::AmountMissing)
    }

    pub fn calculate_total(&mut self) {
        let total = self.available + self.held;
        self.total = total;
    }

    pub fn calculate_locked(&mut self) {
        if self.dispute_counter == 0 {
            self.locked = false
        } else {
            self.locked = true;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic_process() {
        let mut account = Account::default();
        vec![
            TransactionRecord::deposit(account.client, 0, 100.0),
            TransactionRecord::withdrawal(account.client, 1, 10.0),
        ]
            .iter()
            .for_each(|tx| {
                account.process(tx, None).ok();
            });

        assert_eq!(account.available, 90.0);
        assert_eq!(account.held, 0.0);
        assert_eq!(account.total, 90.0);
        assert!(!account.locked);
    }


    #[test]
    fn test_locked() {
        let mut account = Account::default();
        let transactions = vec![
            TransactionRecord::deposit(account.client, 0, 100.0),
            TransactionRecord::withdrawal(account.client, 1, 10.0),
            TransactionRecord::dispute(account.client, 0),
        ];
        for tx in &transactions {
            if tx.kind.as_str() != "deposit" || tx.kind.as_str() != "withdrawal" {
                let additional_tx = transactions.iter()
                    .cloned()
                    .find(|inner| inner.tx == tx.tx);
                account.process(tx, additional_tx).unwrap();
            }
        }

        assert_eq!(account.available, -10.0);
        assert_eq!(account.held, 100.0);
        assert_eq!(account.total, 90.0);
        assert!(account.locked);
    }

    #[test]
    fn test_locked_detailed() {
        let mut account = Account::default();
        let transactions = vec![
            TransactionRecord::deposit(account.client, 0, 100.0),
            TransactionRecord::withdrawal(account.client, 1, 10.0),
            TransactionRecord::dispute(account.client, 1),
            TransactionRecord::resolve(account.client, 1),
        ];

        for tx in &transactions {
            let additional_tx = transactions.iter()
                .cloned()
                .find(|inner| inner.tx == tx.tx);
            account.process(tx, additional_tx).unwrap();
        }

        assert_eq!(account.available, 90.0);
        assert_eq!(account.held, 0.0);
        assert!(!account.locked);
    }
}