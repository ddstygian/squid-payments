use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Csv error")]
    Csv(#[from] csv::Error),
    #[error("Utf 8 conversion error")]
    Utf8Conversion(#[from] std::str::Utf8Error),
    #[error("IO error")]
    Io(#[from] std::io::Error),
    #[error("Glob error")]
    Glob(#[from] glob::GlobError),
    #[error("Additional transaction could not be found")]
    AdditionalTxMissing,
    #[error("The amount of the transaction could not be found")]
    AmountMissing,
}