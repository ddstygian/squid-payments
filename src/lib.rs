use crate::transaction::TransactionRecord;

use std::path::Path;
use crate::csv_util::initialise_raw_csv;
use crate::error::Error;

pub mod transaction;
pub mod account;
pub mod amount;
pub mod error;
pub mod csv_util;

type BoxedWriter = csv::Writer<Box<std::fs::File>>;

pub fn find_additional_transaction(path: &Path, tx: u32) -> Result<Option<TransactionRecord>, Error> {
    let (mut rdr, mut raw_record, headers) = initialise_raw_csv(path)?;
    let mut context_record = None;

    while rdr.read_byte_record(&mut raw_record)? {
        let record_bytes = &raw_record[2];
        let tx_bytes = tx.to_string();
        if let Ok(bytes) = std::str::from_utf8(record_bytes) {
            if bytes == tx_bytes.as_str() {
                let transaction: TransactionRecord = raw_record.deserialize(Some(&headers))?;
                log::trace!("Record: {:?}", transaction);
                context_record = Some(transaction)
            }
        }
    }

    Ok(context_record)
}


#[cfg(test)]
mod tests {
    use std::fs::{File, OpenOptions};
    use std::ops::Range;
    use csv::Writer;
    use rand::{Rng, RngCore};
    use rand::prelude::ThreadRng;
    use tempdir::TempDir;
    use crate::csv_util::{partition, process_csv};
    use super::*;

    const AMOUNT: Range<f32> = 1.0..1000.0;

    #[test]
    fn generate_100m_dataset() {
        let tx_rows: u32 = 100_000_000;
        let path = "tests/transactions_100m.csv";
        let client_range = 1..65535;

        generate_data(tx_rows, path, client_range);
    }

    #[test]
    fn generate_100k_dataset() {
        let tx_rows: u32 = 100_000;
        let path = "tests/transactions_100k.csv";
        let client_range = 1..5000;

        generate_data(tx_rows, path, client_range);
    }
    #[test]
    fn generate_10m_dataset() {
        let tx_rows: u32 = 10_000_000;
        let path = "tests/transactions_10m.csv";
        let client_range = 1..50_000;

        generate_data(tx_rows, path, client_range);
    }

    #[test]
    fn test_partition() {
        let temp = TempDir::new("test").unwrap();
        partition(Path::new("tests/transactions_huge.csv"), temp.path()).unwrap();
    }

    #[test]
    fn test_black_box_100k() {
        process_csv("tests/transactions_100k.csv").unwrap();
    }

    #[test]
    fn test_black_box_10m() {
        process_csv("tests/transactions_10m.csv").unwrap();
    }

    #[test]
    fn test_black_box_100m() {
        process_csv("tests/transactions_100m.csv").unwrap();
    }

    #[test]
    fn test_large() {
        process_csv("tests/transactions_large.csv").unwrap();
    }

    #[test]
    fn test_100m() {
        process_csv("tests/transactions_100m.csv").unwrap();
    }

    fn get_csv_writer(path: &str, should_write_header: bool) -> Writer<File> {
        let file = OpenOptions::new()
            .write(true)
            .create(true)
            .append(true)
            .open(&path)
            .unwrap();
        let mut builder = csv::WriterBuilder::new();
        builder.flexible(true);
        if !should_write_header {
            builder.has_headers(true);
        } else {
            builder.has_headers(false);
        }

        builder.from_writer(file)
    }

    fn generate_data(tx_rows: u32, path: &str, client_range: Range<u16>) {
        let mut should_write_header = true;
        let mut wtr = get_csv_writer(path, should_write_header);

        (1..tx_rows).for_each(|tx| {
            if should_write_header {
                should_write_header = false;
                wtr = get_csv_writer(path, should_write_header);
            }
            let mut rand = rand::thread_rng();

            let client = rand.gen_range(client_range.clone());
            let amount = rand.gen_range(AMOUNT);

            // We generate dispute-like things later
            let transaction = generate_random_transaction(tx, rand, client, amount);

            wtr.serialize(&transaction).unwrap();
        });

        wtr.flush().unwrap();

        let mut should_write_header = true;
        let mut wtr = get_csv_writer(path, should_write_header);

        // Add 10% of problematic transactions
        let range_plus_disputes = tx_rows + (tx_rows / 10);
        (tx_rows..range_plus_disputes).for_each(|_| {
            if should_write_header {
                should_write_header = false;
                wtr = get_csv_writer(path, should_write_header);
            }
            let mut rand = rand::thread_rng();

            let client = rand.gen_range(client_range.clone());
            let transaction = match rand.next_u32() % 2 == 0 {
                true => TransactionRecord::chargeback(client, rand.gen_range(0..tx_rows)),
                false => TransactionRecord::dispute(client, rand.gen_range(0..tx_rows)),
            };
            wtr.serialize(&transaction).unwrap();
        });
        wtr.flush().unwrap();

    }

    fn generate_random_transaction(tx: u32, mut rand: ThreadRng, client: u16, amount: f32) -> TransactionRecord {
        match rand.next_u32() % 2 == 0 {
            true => TransactionRecord::deposit(client, tx, amount),
            false => TransactionRecord::withdrawal(client, tx, amount),
        }
    }
}
