use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Amount(pub f32);

impl Default for Amount {
    fn default() -> Self {
        Amount(0.0)
    }
}

impl Display for Amount {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&format!("{:.4}", self.0))
    }
}

pub mod concrete {
    use serde::{Serializer, Deserializer, Deserialize};
    use std::fmt::Display;
    use crate::amount::Amount;

    pub fn serialize<S, T: Display>(
        amount: &T,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        let s = format!("{}", amount);
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(
        deserializer: D,
    ) -> Result<Amount, D::Error>
        where
            D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        s.parse::<f32>()
            .map_err(serde::de::Error::custom)
            .map(Amount)
    }
}

pub mod optional {
    use serde::{Serializer, Deserializer, Deserialize};
    use std::fmt::Display;
    use crate::amount::Amount;

    pub fn serialize<S, T: Display>(
        amount: &Option<T>,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        if let Some(amount) = amount {
            crate::amount::concrete::serialize(amount, serializer)
        } else {
            serializer.serialize_none()
        }
    }

    pub fn deserialize<'de, D>(
        deserializer: D,
    ) -> Result<Option<Amount>, D::Error>
        where
            D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        s.parse::<f32>().map_err(serde::de::Error::custom).map(|float| Some(Amount(float)))
    }
}
