# 🏦 Squid Payments 🏦

The purpose of this is to aggregate a csv of transactions at the command line and spit out account data.

## Completeness

I've chosen to use a library so that the api could be benchmarked with the binary being a thin wrapper around the lib crate.

I'm quite sure the data that is returned is accurate. There are some unit tests to account for this in the account module.

For formatting there is some complexity around writing a specific format, for this I've chosen to use a custom deserializer.
All of this is located in the account module. Im certain that it is not quite perfect for all use cases, but works in this case.


## Correctness

I've written a quick test to generate various amounts of data, they are located in the `lib` module and the dataset is stored in
the `tests/` directory.
I also tested with 100million transactions but the file is too large to upload it, it can however easily be created using the generation function.

~I've chosen to use ADT/Sum types to guarantee completeness and ease of extension.~
I had initially chosen to use the above, but in the efforts of mutating in place to speed things up, I chose to work on the record directly.
This has added benefits, but I think the cost is worth it. 

In terms of danger, there are some places where a panic may happen. 
Ultimately, the application can not go on without a reader, so it's safe to expect the program to panic there.


## Efficiency

Using criterion-rs, a few benchmarks were run, the first was a benchmark of 100k transactions and some ~5k dispute/resolutions, 
this benchmark was my baseline as I moved from allocated > less allocation > sharded and finally parallel sharded.

So far unoptimised it took about 8mins to handle 10m records with about 50k accounts. Of course this is not really a real world example
due to most accounts doing maybe 1k transactions a year, this is also without optimisations.

With release mode it took 2mins and 30 seconds.

This was the trickiest part of the test as I had to peer records to keep it as copy-less as possible whilst still being able to search for transactions when
a dispute-like transaction occurred. 

I have taken the sharding approach as I assumed that a reasonable amount of clients would enable this to chunk quite nicely. 
Of course, the main drawback for this would be if there were `MAX_U32` transactions assigned to very few users. 
I was not able to make many assumptions on the assessment dataset so assumed a real world use case where a user would have some 100k transactions or so each but for fun
I also dabbled a bit with much larger datasets, such as 100million, and one billion, but I was running out of IO space on my laptop!

The sharding key was quite simple, just the client id. 
You could, if you wanted to optimise it even further, find a nicer shard key but that is more of a vertical scale if your OS wouldn't support many open files.

An optimisation on this if you didn't want multiple files could be to merge buckets into a reasonable structure. 
Or if you were adamant on this approach you could ask the OS what a reasonable number of files would be and then shard that.
If I knew the test data then I would devise the shard key accordingly.

I have also dabbled with generating MAX_U32 transactions it took a while to generate. 

I think there is a possibility that this could be much better, but for the life of me I could not think of a way to peer WHILST needing to lookup
the transactions for disputes and such. I think an index *may* be quicker or scale better, but I'm not sure. I did spend most of the time I spent on this
thinking of marvellous ways to zero-allocate but this was the best way I could think of. I do however respect that someone who knows the gotcha or perhaps
a data scientist could whip a good idea up.

# Note on input command

I had a careful eye when I was looking through the PDF and I noticed that the command line input did not specify release mode.
I have monkey patched the config file in the TOML to have a release-like mode for default too, so we can get free optimisations from the compiler.

If this was not allowed, please forgive me! :)